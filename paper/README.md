<div align = "center">
<h1>Paper description</h1>
</div>
Papers description follow
<br>
<br>
<div align = "center">
<h3>Ontology</h3>
</div>

- Stuart J. Chalk, (2016). <a href = "https://gitlab.com/DBertazioli/meetology/blob/master/paper/SciData%20a%20data%20model%20and%20ontology%20for%20semantic%20representation%20of%20scientific%20data.pdf"><strong>"SciData: a data model and ontology for semantic representation of scientific data"</strong></a>, Journal of Cheminformatics, 8 (1), 1.


<p>Article about the use of JSON-LD in creating an ontology for scientific data, in particular chemistry.</p>
<br>
<br>
<br>
<div align = "center">
<h3>Description categories</h3>
</div>

- Alper Kursat Uysal and Serkan Gunal, (2014). <a href = "https://gitlab.com/DBertazioli/meetology/blob/master/paper/The%20impact%20of%20preprocessing%20on%20text%20classification.pdf"><strong>"The impact of preprocessing on text classification"</strong></a>, Information Processing & Management, 50 (1), 104-112, ISSN 0306-4573.
<p>Paper about the influence of widely used preprocessing tasks on text classification examined in two different domains and languages.</p>
<br>

- D. Xue and F. Li, (2015). <a href  = "https://gitlab.com/DBertazioli/meetology/blob/master/paper/Random_forest_text_categorization_models.pdf"><strong>"Research of Text Categorization Model based on Random Forests"</strong></a>,  2015 IEEE International Conference on Computational Intelligence & Communication Technology, Ghaziabad, 173-176.
<p>This paper first introduced random forests then establish a text categorization model based on random forest and design the experiment to evaluate its performance.</p>
<br>

- David M. Blei, Andrew Y. Ng, Michael I. Jordan, (2003). <a href = "https://gitlab.com/DBertazioli/meetology/blob/master/paper/LDA.pdf"><strong>"Latent dirichlet allocation"</strong></a>, The Journal of Machine Learning Research, 3, 993-1022. 
<p>Simplified explanations for this argument are available <a href = "http://www.andreaminini.com/semantica/latent-dirichlet-allocation-lda#come_funziona_l'allocazione_lda">here</a> and <a href = "https://towardsdatascience.com/light-on-math-machine-learning-intuitive-guide-to-latent-dirichlet-allocation-437c81220158">here</a>. This paper describe latent Dirichlet allocation, a flexible generative probabilistic model for collections of discrete data, based on a simple exchangeability assumption for the words and topics in a document.</p>
<br>

-  Le Q. and Mikolov T. , (2014). <a href = "https://gitlab.com/DBertazioli/meetology/blob/master/paper/paragraph_vector.pdf"><strong>"Distributed Representations of Sentences and Documents"</strong></a>, Proceedings of the 31st International Conference on Machine Learning, in PMLR, 32(2), 1188-1196.
<p>This paper is about Paragraph Vector (future doc2vec), an unsupervised learning algorithm that learns vector representations for variable-length pieces of texts such as sentences and documents. The vector representations are learned to predict the surrounding words in contexts sampled from the paragraph.</p>
